data "docker_registry_image" "Mylar3" {
	name = "linuxserver/mylar3:latest"
}

resource "docker_image" "Mylar3" {
	name = data.docker_registry_image.Mylar3.name
	pull_triggers = [data.docker_registry_image.Mylar3.sha256_digest]
}

module "Mylar3" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Mylar3.latest

	networks = [{ name: var.docker_network, aliases: ["mylar3.${var.internal_domain_base}"] }]
  ports = [{ internal: 8090, external: 9604, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Mylar3"
			container_path = "/config"
			read_only = false
		},
		{
			host_path = "${var.comics}"
			container_path = "/comics"
			read_only = false
		},
		{
			host_path = "${var.downloads}"
			container_path = "/downloads"
			read_only = false
		}
	]

	environment = {
		"PUID": "${var.uid}",
		"PGID": "${var.gid}",
		"TZ": "${var.tz}"
	}

	stack = var.stack
}